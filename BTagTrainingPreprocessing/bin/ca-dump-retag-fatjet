#!/usr/bin/env python3

from argparse import ArgumentParser
import sys

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg as getConfig
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaConfiguration.AllConfigFlags import initConfigFlags
from BTagTrainingPreprocessing import dumper
from BTagTrainingPreprocessing import retagfatjet

def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    track_opts = parser.add_mutually_exclusive_group()

    track_opts.add_argument(
        "-M",
        "--merge-tracks",
        action="store_true",
        help="merge the standard and large d0 track collections before everything else.\n"
             "only works in releases after 22.0.59 (2022-03-08T2101)",
    )

    track_opts.add_argument(
        "-t",
        "--track-sys",
        help="list of tracking systematics to apply. For the full list of options\n"
             "see InDetTrackSystematicsTools/InDetTrackSystematics.h",
        nargs="+",
        type=str,
    )

    return parser.parse_args()

def run():

    args = get_args()

    track_collection="InDetTrackParticles",
    
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"
    cfgFlags = initConfigFlags()
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()
    ca = getConfig(cfgFlags)
    ca.merge(PoolReadCfg(cfgFlags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    
    
    ca.merge(
        retagfatjet.AddTrackSys_largeR(            
            track_collection,
            args.track_sys,
            jet_collection,
        )
    )

    

    ca.merge(dumper.getDumperConfig(args))


    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
