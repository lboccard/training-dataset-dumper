#ifndef TRIGGER_VR_JET_DECORATOR_TOOL_H
#define TRIGGER_VR_JET_DECORATOR_TOOL_H

#include "AsgTools/AsgTool.h"
#include "JetInterface/IJetDecorator.h"
#include "xAODJet/JetContainerFwd.h"

class VRJetOverlapDecorator;

class TriggerVRJetOverlapDecoratorTool : public asg::AsgTool, virtual public IJetDecorator
    {
        ASG_TOOL_CLASS(TriggerVRJetOverlapDecoratorTool, IJetDecorator)
        public:
            TriggerVRJetOverlapDecoratorTool(const std::string& name);
            ~TriggerVRJetOverlapDecoratorTool();

            StatusCode initialize() override;

            //returns 0 for success
            StatusCode decorate(const xAOD::JetContainer& jets) const override;
        private:
            std::unique_ptr<VRJetOverlapDecorator> m_dec;
    };
#endif
